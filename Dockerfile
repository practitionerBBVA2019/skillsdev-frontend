#Para subir a dockerhub ejecutar
#
#polymer build
#sudo docker build --no-cache -t dilvergutierrez/nginx .
#sudo docker run -d -p 8000:80 dilvergutierrez/nginx
#sudo docker push dilvergutierrez/nginx
#
#sudo docker ps -a
#sudo docker stop [id]

FROM nginx
#info:    Clearing build/ directory...
#info:    (es6-unbundled) Building...
#info:    (es6-bundled) Building...
#info:    (es5-bundled) Building...
#info:    (es6-unbundled) Build complete!
#info:    (es6-bundled) Build complete!
#info:    (es5-bundled) Build complete!
#COPY ./build/es6-bundled /usr/share/nginx/html
RUN rm -frv /usr/share/nginx/html/*
RUN ls /usr/share/nginx/html/
COPY ./build/es6-unbundled /usr/share/nginx/html
RUN ls /usr/share/nginx/html/
